const express = require('express')
const app = express()
const {greeting} = require('./user')

app.get('/', (req,res) => res.send('This App is running properly!'))
app.get('/ping', (req,res) => res.send('Pong!'))
app.get('/me', (req,res) => res.send('I am Me!'))
app.get('/home', (req,res) => res.send('My Home!'))
app.get('/youtube', (req,res) => res.send('Hello, youtube indonesia!'))
app.get('/hello/:name', (req,res) => {
    res.json({message:greeting(req.params.name)})
})
module.exports = app
